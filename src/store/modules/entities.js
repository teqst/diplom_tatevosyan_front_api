/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */
//
import {getProducts, getNews, getCategories, getPartners} from "@/API/entities.js";


export default {
    namespaced: true,
    state: {
        products: [],
        partners: [],
        news: [],
        categories: [],
    },
    mutations: {
        SET_PRODUCTS(state, products) {
            state.products = products;
        },
        SET_NEWS(state, news) {
            state.news = news;
        },
        SET_PARTNERS(state, partners) {
            state.partners = partners;
        },
        SET_CATEGORIES(state, categories) {
            state.categories = categories;
        },
    },
    actions: {
        async getProducts({commit, getters}) {
            if(getters.allProducts.length === 0) {
                const resp = await getProducts()
                commit('SET_PRODUCTS', resp.data)
            }
        },
        async getNews({commit, getters}) {
            if(getters.allNews.length === 0) {
                const resp = await getNews()
                commit('SET_NEWS', resp.data)
            }
        },
        async getPartners({commit, getters}) {
            if(getters.allPartners.length === 0) {
                const resp = await getPartners()
                commit('SET_PARTNERS', resp.data)
            }
        },
        async getCategories({commit, getters}) {
            if(getters.allCategories.length === 0) {
                const resp = await getCategories()
                commit('SET_CATEGORIES', resp.data)
            }
        },
        // async getSortedByPriceProducts({commit}, by) {
        //     const resp = await getProducts()
        //     switch (by) {
        //         case 'DESC':
        //             commit('SET_PRODUCTS', resp.data['hydra:member'].sort((a, b) => a.price < b.price ? 1 : -1))
        //             break;
        //         case 'ASC':
        //             commit('SET_PRODUCTS', resp.data['hydra:member'].sort((a, b) => a.price > b.price ? 1 : -1))
        //             break;
        //     }
        // },
        // async getSortedByNameProducts({commit}) {
        //     const resp = await getProducts()
        //     let sorted = resp.data['hydra:member'].sort();
        //     sorted.reverse();
        //     commit('SET_PRODUCTS', sorted)
        // },
        // async getFilteredByPrice({commit}, settledPrice) {
        //     const resp = await getProducts()
        //     let filtered = resp.data['hydra:member'].filter(item => item.price < settledPrice);
        //     commit('SET_PRODUCTS', filtered)
        // }
    },
    getters: {
        allProducts(state) {
            return state.products
        },
        allNews(state) {
            return state.news
        },
        allPartners(state) {
            return state.partners
        },
        allCategories(state) {
            return state.categories
        }
    }
}