import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import News from "../views/News.vue";
import NewsDetail from "@/views/NewsDetail";
import Partners from "../views/Partners.vue";
import NotFound from "@/views/NotFound";
import Contact from "@/views/Contact";
import Catalog from "@/views/Catalog";
import Categories from "@/views/Categories";
import Cart from "@/views/Cart";
import ItemDetail from "@/views/ItemDetail";
import store from "@/store";
import CategoryItems from "@/views/CategoryItems";
import Search from "@/views/Search";
import SignUp from "@/views/SignUp";
import SignIn from "@/views/SignIn";
import Profile from "@/views/Profile";
import Checkout from "@/views/Checkout";
import Success from "@/views/Success";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      key: "Главная",
    },
  },
  {
    path: "/news",
    name: "News",
    component: News,
    meta: {
      key: "Новости",
    },
  },
  {
    path: "/cart",
    name: "cart",
    component: Cart,
    meta: {
      key: "Корзина",
    },
  },
  {
    path: "/Cart/Checkout/",
    name: "Checkout",
    component: Checkout,
    meta: {
      key: "Оформление заказа",
      requireLogin: true,
    }
  },
  {
    path: "/Cart/Success/",
    name: "Success",
    component: Success,
    meta: {
      key: "Заказ оформлен",
      requireLogin: true,
    }
  },
  {
    path: "/news/:guid",
    name: "NewsDetail",
    component: NewsDetail,
    meta: {
      key: "Подробнее о новости",
    },
  },
  {
    path: "/:category_slug/:product_slug/",
    name: "ItemDetail",
    component: ItemDetail,
    meta: {
      key: "Подробнее о товаре",
    },
  },
  {
    path: "/partners",
    name: "Partners",
    component: Partners,
    meta: {
      key: "Партнеры",
    },
  },
  {
    path: "/contacts",
    name: "Contact",
    component: Contact,
    meta: {
      key: "Контакты",
    },
  },
  {
    path: "/catalog",
    name: "catalog",
    component: Catalog,
    meta: {
      key: "Каталог",
    },
  },
  {
    path: "/search",
    name: "search",
    component: Search,
    meta: {
      key: "Поиск",
    },
  },
  {
    path: "/signup",
    name: "signup",
    component: SignUp,
    meta: {
      key: "Регистрация",
    },
  },
  {
    path: "/signin",
    name: "signin",
    component: SignIn,
    meta: {
      key: "Авторизация",
    },
  },
  {
    path: "/profile",
    name: "profile",
    component: Profile,
    meta: {
      key: "Ваш профиль",
      requireLogin: true,
    },
  },
  {
    path: "/categories",
    name: "categories",
    component: Categories,
    meta: {
      key: "Категории",
    }
  },
  {
    path: "/:category_slug/",
    name: "CategoryItems",
    component: CategoryItems,
    meta: {
      key: "Товары категории",
    },
  },
  {
    path: "*",
    name: "notFound",
    component: NotFound,
    meta: {
      key: "Не найдено",
    },
  },
];


const router = new VueRouter({
  mode: "hash",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  store.commit("setState", {
    key: "mobileMenuVisible",
    value: false,
  });

  if (to.matched.some(record => record.meta.requireLogin) && !store.state.isAuthenticated) {
    next({name: 'signin', query: {to: to.path }});
  } else {
    next()
  }
  });

router.afterEach(() => {
  window.scrollTo(0, 0);
});

export default router;
