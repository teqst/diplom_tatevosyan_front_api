/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

import {instance} from "./utils";

export const getProducts = () => instance.get('/latest-products');
export const getProductBySlug = (category_slug, product_slug) =>
    instance.get(`/products/${category_slug}/${product_slug}`);
export const getCategoryProductsBySlug = (category_slug) =>
    instance.get(`/products/${category_slug}/`);
export const getPartners = () => instance.get('/all-partners');
export const getNews = () => instance.get('/all-news');
export const getCategories = () => instance.get('/all-categories');
export const getMyOrders = () => instance.get('/orders/');

export const doSearch = (query) => instance.get('/products/search/?query=' + query);

export const register = (formdata) => instance.post('/users/', formdata);

export const login = (formdata) => instance.post('/token/login/', formdata);

export const checkout = (formdata) => instance.post('/checkout/', formdata);

export const consultation = (formdata) => instance.post('/consultation_request/', formdata);
