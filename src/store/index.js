import Vue from "vue";
import Vuex from "vuex";
import entities from '@/store/modules/entities'


Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    mainMenu: [
      {
        id: "main",
        name: "Главная",
        to: "/",
      },
      {
        id: "catalog",
        name: "Каталог",
        to: "/catalog",
      },
      {
        id: "partners",
        name: "Партнеры",
        to: "/partners",
      },
      {
        id: "news",
        name: "Новости",
        to: "/news",
      },
      {
        id: "categories",
        name: "Категории",
        to: "/categories",
      },
    ],
    sideMenu: [
      {
        id: "main-screen-first",
        name: "Первый экран",
      },
      {
        id: "main-screen-news",
        name: "Новости и анонсы",
      },
      {
        id: "main-screen-about",
        name: "Наш магазин",
      },
      {
        id: "main-screen-cost",
        name: "Услуги",
      },
      {
        id: "main-screen-mentors",
        name: "Наша команда",
      },
      {
        id: "main-screen-about-qatar",
        name: "О нас",
      },
      {
        id: "main-screen-contacts",
        name: "Контакты",
      },
    ],
    visibleSection: "",
    mobileMenuVisible: false,
    isTablet: false,
    isMobile: false,
    sideBar: true,
    CTAFormSuccess: false,
    lessonType: '',


    cart: {
      items: []
    },
    shippingMethod: '',

    loading: false,
    isAuthenticated: false,
    token: '',

  },
  mutations: {
    initStore(state) {
      if (localStorage.getItem('cart')) {
        state.cart = JSON.parse(localStorage.getItem('cart'));
      } else {
        localStorage.setItem('cart', JSON.stringify(state.cart))
      }

      if (localStorage.getItem('token')) {
        state.token =  JSON.parse(localStorage.getItem('token'));
        state.isAuthenticated = true;
      } else {
        state.token = '';
        state.isAuthenticated = false;
      }
    },
    addToCart(state, item) {
      const exists = state.cart.items.filter(i => i.product.id === item.product.id);

      if (exists.length) {
        exists[0].quantity = parseInt(exists[0].quantity) + parseInt(item.quantity);
      } else {
        state.cart.items.push(item);
      }

      localStorage.setItem('cart', JSON.stringify(state.cart));
    },
    deleteFromCart(state, item) {
      const exists = state.cart.items.filter(i => i.product.id === item.product.id);

      if (exists.length) {
        state.cart.items.find((i) => {
          if (i.product.id === item.product.id) {
            i.quantity === 1 ? state.cart.items = state.cart.items.filter(com => com.product.id !== item.product.id) : i.quantity--;
          }
        })
      }

      localStorage.setItem('cart', JSON.stringify(state.cart));
    },
    SET_LOADING(state, loading) {
      state.loading = loading;
    },
    CLEAR_CART(state) {
      state.cart = {items: []};
      localStorage.removeItem('cart');
    },
    SET_TOKEN(state, token) {
      state.token = token;
      state.isAuthenticated = true;
      localStorage.setItem('token', JSON.stringify(state.token));
    },
    REMOVE_TOKEN(state) {
      state.token = '';
      state.isAuthenticated = false;
    },
    setState(state, payload) {
      state[payload.key] = payload.value;
    }
  },
  actions: {

  },
  modules: {
    entities,
  },
  getters: {
    isLoading(state) {
      return state.loading;
    }
  }
});
