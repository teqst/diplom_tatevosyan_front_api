import Vue from "vue";
import VueScrollTo from "vue-scrollto";
import lineClamp from "vue-line-clamp";
import Vuelidate from "vuelidate";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

Vue.use(VueScrollTo, {
  onStart: function() {
    store.commit("setState", {
      key: "mobileMenuVisible",
      value: false,
    });
  },
});

Vue.use(lineClamp);

Vue.use(Vuelidate);

new Vue({
  render: (h) => h(App),
  router,
  store,
  mounted: () => document.dispatchEvent(new Event("x-app-rendered")),
}).$mount("#app");
